package com.example.model;

import lombok.Data;

/**
 * Created by kax on 30/04/16.
 */
@Data
public class UrlMessage {

    Long id;
    String url;
    String status;
}
