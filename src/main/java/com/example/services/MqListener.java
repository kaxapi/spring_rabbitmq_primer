package com.example.services;

import com.example.consts.Queues;
import com.example.model.UrlMessage;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by kax on 30/04/16.
 */
@Service
public class MqListener {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @RabbitListener(queues = "rmq_test_queue")
    public void processQueue1(UrlMessage message) {
        System.out.println(message);
        rabbitTemplate.convertAndSend(Queues.QUEUE_OUTPUT, message);

    }
}
