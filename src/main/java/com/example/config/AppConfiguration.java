package com.example.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by kax on 30/04/16.
 */
@EnableAutoConfiguration
@ComponentScan
public class AppConfiguration {
}
