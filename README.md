# README #

Minimal primer Spring Boot RabbitMQ application with JSON serializer/deserializer.
Can be used with Python pika (pip3 install pika) with json.loads/json.dumps
For more information, see https://www.rabbitmq.com/getstarted.html